import { useState} from 'react'
import axios from 'axios'

const Users = () => {
    const [ users, getUsers ] = useState([])
    const token = window.localStorage.getItem('authToken')

    axios
        .get("https://ka-users-api.herokuapp.com/users", {headers:{Authorization: token}})
        .then(res => getUsers(res.data))
        .catch(err => console.log(err))

    return (
        <div>
        </div>
    )
}

export default Users;