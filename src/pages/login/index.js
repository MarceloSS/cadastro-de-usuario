import { useForm } from 'react-hook-form';
// import { useState } from 'react';
import axios from 'axios';

const Login = (props) => {
    const { register, handleSubmit } = useForm();
    // const [ error, setError ] = useState({});

    const tryLogin = (data) => {
        console.log(data);
        // setError({});
        axios
            .post( "https://ka-users-api.herokuapp.com/authenticate", data)
            .then((res) => {
                console.log(res)
                window.localStorage.setItem("authToken", res.data.auth_token);
                props.history.push('/users');
            }).catch((err) => {
                // {err.responde && setError(err.response.data)}
                console.log(err)
            });
    }

    return (
        <div className="App">
            <header className="App-header">
                <form onSubmit={ handleSubmit(tryLogin) }>
                    <div>
                        <input placeholder="Usuario" name ="user" ref={ register } ></input>
                    </div>
                    <div>
                        <input placeholder="Senha" name="password" ref={ register }></input>
                    </div>
                    {/* {error.error && <span>Credenciais invalidas</span>} */}
                    <div>
                        <button type="submit">Enviar</button>
                    </div>
                </form>
            </header>
        </div>
    );
}

export default Login;