import { useForm } from 'react-hook-form';
import { useState} from 'react'
import axios from 'axios'
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup'; 

const UserForm = (props) => {
    const [error, setError] = useState({})

    const tryRegister = (data) => {
        setError({})
        axios
            .post( "https://ka-users-api.herokuapp.com/users", {user: data})
            .then(res => console.log(res), props.history.push('/login'))
            .catch((err) => {
                {err.responde && setError(err.response.data)}
            });
    }

    const scheme = yup.object().shape({
        user : yup.string().min(6, "Minimo 4 digitos").required("Campo obrigatorio"),
        name : yup.string().min(4, "Minimo 4 digitos").required("Campo obrigatorio"),
        email : yup.string().email("Email invalido").required("Campo obrigatorio"),
        password : yup.string().min(6, "Minimo de 6 digitos").required("Campo obrigatorio"),
        passwordConfirm : yup.string().oneOf([yup.ref("password")], "Senhas diferentes").required(),
    });

    const {register, handleSubmit, errors} = useForm(
        { resolver : yupResolver(scheme) }
    );

    return (
        <div className="App">
            <header className="App-header">

                <form onSubmit={ handleSubmit(tryRegister) }>
                    <div>
                        <input placeholder="Usuário" name ="user" ref={ register } ></input>
                        <p style={ {color:"red"} }> { errors.name?.message } </p>
                    </div>
                    <div>
                        <input placeholder="Nome" name ="name" ref={ register } ></input>
                        <p style={ {color:"red"} }> { errors.name?.message } </p>
                    </div>
                    <div>
                        <input placeholder="Email" name ="email" ref={ register } ></input>
                        <p style={ {color:"red"} }> { errors.email?.message } </p>
                    </div>
                    <div>
                        <input placeholder="Senha" name="password" ref={ register }></input>
                        <p style={ {color:"red"} }> { errors.password?.message } </p>
                    </div>
                    <div>
                        <input placeholder="Confirmar Senha" name ="passwordConfirm" ref={ register } ></input>
                        <p style={ {color:"red"} }> { errors.passwordConfirm?.message } </p>
                    </div>
                    {error.user && <span>Esse usuario ja foi cadastrado</span>}
                    {error.email && <span>Esse email ja foi cadastrado</span>}
                    <div>
                        <button type="submit">Enviar</button>
                    </div>
                </form>
            </header>
        </div>
    );
}

export default UserForm;