import './App.css';
import { useState, useEffect } from 'react';
import { Switch, Route, useHistory } from 'react-router-dom';

import UserForm from './pages/userForm';
import Login from './pages/login';
import Users from './pages/users';

function App() {  
  const history = useHistory();

  const [ token, setToken ] = useState('')

  const logOut = () => {
    localStorage.removeItem('authToken');
    history.push('/login');
  }

  useEffect(() => {
    setToken(localStorage.getItem("authToken"))
  }, []);


  return (
    <div className="App">
      <header className="App-header">
        <div style={{position:'absolute', top:'0'}}>
          <span><button onClick={() => history.push('/')}>Home</button></span>
          {token !== null ?
              <span>
                <button onClick={() => history.push('/users')}>Users</button>
                <button onClick={() => history.push('/userFeedbacks')}>FeedBacks</button>
                <button onClick={() => history.push('/feedbackForm')}>FeedBack Form</button>
              </span>
            :
              <span>
                <button onClick={() => history.push('/userForm')}>Register</button> 
                <button onClick={() => history.push('/login')}>Login</button>
              </span>
          }
          {token !== null &&               
          <button style={{position:'absolute', top:'10px', right:'-425px'}} onClick={logOut}>Out</button>
          }
        </div>
          <Switch>
            <Route exact path='/'>
              <h1>Welcome!</h1>
            </Route>

            <Route path='/users'>
              <Users/>
            </Route>

            <Route path='/userFeedbacks'>
              
            </Route>

            <Route path='/feedbackForm'>
              
            </Route>

            <Route path='/userForm'>
              <UserForm history={history}/>
            </Route>

            <Route path='/login'>
              <Login history={history}/>
            </Route>
          </Switch>
        
      </header>
    </div>
  );
}

export default App;
